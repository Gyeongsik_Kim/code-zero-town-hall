/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.chipsen.cle110_test_kit;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.chipsen.cle110_test_kit";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
}
