package com.chipsen.cle110_test_kit;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
public class BroadcastReceive extends BroadcastReceiver {
	  public void onReceive(Context context, Intent intent) {
              String action = intent.getAction();
              if(action.equals("android.intent.action.BOOT_COMPLETED")) {
                      Intent i = new Intent(context, chipsen_logo.class);
                      i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                      context.startActivity(i);
              }
      }
}
