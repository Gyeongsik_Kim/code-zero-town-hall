/*<저작권>
본 프로그램 및 소스코드는 (주)칩센의 저작물입니다.
본 프로그램 및 소스코드는 (주)칩센의 블루투스 제품을 구입한 고객에게 제공되는 것 입니다.
당사의 블루투스 제품을 활용할 목적 이외의 용도로 사용하는 것을 금지합니다.

<License>
The program or internal source codes was created by CHIPSEN Co.,Ltd.
In order to use the program or internal source codes, you must buy CHIPSEN's Bluetooth products.
You are not allowed to use it for purposes other than CHIPSEN's Bluetooth Products

Copyright 2015. CHIPSEN all rights reserved.*/

package com.chipsen.cle110_test_kit;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Window;

public class chipsen_logo extends Activity {
	  protected static final String LOG_TAG = null;
	private Handler mHandler;
	    private Runnable mRunnable;
	 /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.chipsen_logo);
        
        
        mRunnable = new Runnable() {
            @Override
            public void run() {
            	Log.e(LOG_TAG, "parkchipsen_logo=+++ ON CREATE +++1_1");
                Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
               startActivity(intent);
                finish();
            }
        };
         
        mHandler = new Handler();
        mHandler.postDelayed(mRunnable,1000);
        
		  
      
        // TODO Auto-generated method stub
    }
    @Override
    protected void onDestroy() {
        mHandler.removeCallbacks(mRunnable);
        super.onDestroy();
    }

}
