#define HUMAN 3
#define TEMP A3
#define RADIO 4
#define SPEAKER 10
#define CALL 11
#define SENSOR 3
#define TEST 12
#define FIRE A3
bool isHuman, isFire, isOut;
short temp;

void setup(){
  Serial.begin(1200);
  pinMode(HUMAN, INPUT);
  pinMode(TEMP, INPUT);
  pinMode(SENSOR, INPUT_PULLUP);
  pinMode(RADIO, INPUT);
  pinMode(SPEAKER, OUTPUT);
  pinMode(CALL, OUTPUT);
  pinMode(TEST, OUTPUT);
  isFire = isHuman = isOut = false;
}

void loop(){
  String input = "";
  short fire = analogRead(FIRE);
  temp = fire;
  if(Serial.available()){
      input += (char)Serial.read();
      if(input == "p"){
        digitalWrite(SPEAKER, HIGH);
        digitalWrite(CALL, LOW);
      }else if(input == "s"){
        digitalWrite(SPEAKER, HIGH);
        digitalWrite(CALL, HIGH);
      }else if(input == "S"){
        digitalWrite(SPEAKER, LOW);
        digitalWrite(CALL, LOW);
      }else if(input == "o"){
        isOut = true;
        digitalWrite(SPEAKER, HIGH);  
        digitalWrite(CALL, LOW);
      }else if(input == "O"){
        isOut = false;
      }
  }else{
    if(fire < 150){
      isFire = true;
    Serial.print("F");
     digitalWrite(SPEAKER, HIGH);
     digitalWrite(CALL, HIGH);
  }else if(isFire && (temp < 150 || fire >150)){
    Serial.print("f");
    isFire = false;
     digitalWrite(SPEAKER, LOW);
     digitalWrite(CALL, LOW);
  }
if(digitalRead(HUMAN) == HIGH){
    Serial.print("h");
    isHuman = true;
    if(isOut){
       digitalWrite(SPEAKER, HIGH);
        digitalWrite(CALL, HIGH);
      }
    }else if(isHuman){ 
    Serial.print("H");
    isHuman = false;
    }
  }
}

  

/*
 * p : 재생
 * c : 방송
 * s : 안심콜
 */ 
